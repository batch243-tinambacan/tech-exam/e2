let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array

   return collection
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method

    collection[collection.length] = element
    return collection
}

function dequeue() {
    // In here you are going to remove the first element in the array
    for (let i = 1; i < collection.length; i++) collection[i - 1] = collection[i];

    collection.length--;
    return collection
}

function front() {
    // In here, you are going to get the first element

    return collection[0]
}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements 
    let count = 0
    collection.forEach(_=> count++)
    return count  
}

function isEmpty() {
    //it will check whether the array is empty or not
    if (collection !== null){
        return false
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};